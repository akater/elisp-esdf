;; -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'esdf
  authors "Dima Akater"
  first-publication-year-as-string "2022"
  org-files-in-order '( "eieio-akater-extras"

                        "esdf-macs"

                        "mary-sue-pattern"
                        "cl-pathname"

                        "esdf-util" "esdf-core" "esdf-elisp"
                        "esdf-literate" "esdf-org"

                        "esdf-tests"
                        ;; "esdf-emerge"
                        "esdf")
  site-lisp-config-prefix "50"
  license "GPL-3")

