;;;  -*- lexical-binding: t -*-

(defun file-directory-p+ (filename)
  "When (FILE-DIRECTORY-P FILENAME) is non-nil, return FILENAME."
  (when (file-directory-p filename) filename))

;; file-exists-p+ is defined in fakemake-done

(defun stringp+ (object)
  "When (STRINGP OBJECT) is non-nil, return OBJECT."
  (when (stringp object) object))

(defun functionp+ (object)
  "When (FUNCTIONP OBJECT) is non-nil, return OBJECT."
  (when (functionp object) object))

(defun read-list (string-or-nil)
  (when string-or-nil
    (with-temp-buffer
      (insert ?\( string-or-nil ?\))
      (goto-char (point-min))
      (read (current-buffer)))))

(require 'cl-macs)

(when (< emacs-major-version 28)
  (cl-deftype keyword () `(satisfies keywordp)))

(defun ensure-string (x)
  (cl-etypecase x
    (string x)
    (keyword (substring (symbol-name x) 1))
    (symbol (symbol-name x))
    (number (number-to-string x))))

(defun keyword (symbol-or-string)
  "Convert SYMBOL-OR-STRING to keyword."
  (cl-etypecase symbol-or-string
    (keyword symbol-or-string)
    (symbol (keyword (symbol-name symbol-or-string)))
    (string (intern (concat ":" symbol-or-string)))))

(defmacro plist-with-keys (&rest symbols)
  (cons 'list (cl-loop for s in symbols collect (keyword s) collect s)))

(defmacro npush-pop (source target)
  "Like (push (pop SOURCE) TARGET) but reuse the cons cell, and return nil."
  `(cl-rotatef ,source (cdr ,source) ,target))

(require 'cl-seq)

(defun fold (f x list) (cl-reduce f list :initial-value x))

(defmacro fif (test f x)
  "If TEST evaluates to non-nil, return (F X).  Otherwise, return X."
  (declare (indent 1))
  `(if ,test (funcall ,f ,x) ,x))

(defun string-matched-p (x y) (string-match-p y x))

(cl-defmacro do-files-list ((var list &optional return)
                            ;; (var list &key return (if-not-exists 'message))
                            &body body)
  "Evaluate BODY for each VAR in LIST, presuming VAR to be relative file name.

If BODY starts with a string, it is presumed to be format strings with two %s slots: length, plural/singular -s

VAR can be (var description); description will be evaluated each time
`do-not-count' is called, and if there's format-string, in the end too.

The arglist of do-not-count is (&optional format-string args)."
  (declare (indent 1))
  (let (description)
    (cl-assert (or (symbolp var) (prog1 (and (consp var)
                                             (prog1 (consp (cdr var))
                                               (setq description
                                                     (cadr var)))
                                             (null (cddr var)))
                                   (setq var (car var)))))
    (let* ((do-not-count-g (gensym "do-not-count-"))
           (format-string (stringp+ (car body)))
           (count-g (when format-string (gensym "count-")))
           (body (fif format-string #'cdr body)))
      (setq description (or description "file"))
      (fif format-string
        (lambda (form)
          `(let ((,count-g 0))
             (prog1 ,form
               (message ,format-string
                        ,count-g (if (= 1 ,count-g) "" "s")))))
        `(dolist (,var ,list ,return)
           ,(fif format-string (lambda (form)
                                 `(let (,do-not-count-g)
                                    ,form
                                    (unless ,do-not-count-g
                                      (cl-incf ,count-g))))
                 `(cl-flet (,(let ((local-format-string
                                    (gensym "format-string-"))
                                   (args (gensym "args-")))
                               `(do-not-count
                                 (&optional ,local-format-string &rest ,args)
                                 (when ,local-format-string
                                   (cl-check-type ,local-format-string string)
                                   (apply #'message
                                          (concat
                                           "Skipping " (ensure-string
                                                        ,description)
                                           (if (and (cl-plusp
                                                     (length ,local-format-string))
                                                    (not
                                                     (or (char-equal
                                                          ?\s
                                                          (aref ,local-format-string 0))
                                                         (char-equal
                                                          ?\t
                                                          (aref ,local-format-string 0)))))
                                               " %s "
                                             " %s")
                                           ,local-format-string)
                                          ,var ,args))
                                 ,@(when format-string
                                     `((setf ,do-not-count-g t)))
                                 nil)))
                    (cond
                     ((not (file-exists-p ,var))
                      (message (concat "Skipping non-existing " (ensure-string
                                                                 ,description)
                                       " %s")
                               ,var))
                     (t ,@body)))))))))

(cl-defmacro do-default-directory-files ((var match
                                              &optional full nosort return)
                                         &body body)
  "Evaluate BODY for each file in default-directory matching MATCH.

VAR is bound to corresponding relative file name.

FULL, NOSORT work as in `directory-files'.

RETURN works as in `dolist'.

Macroexpands to do-files-list."
  (declare (indent 1))
  `(do-files-list (,var (directory-files default-directory
                                         ,full ,match ,nosort)
                        ,return)
     ,@body))

(cl-defmacro do-files-recursively ((var root &key do-not-descend return)
                                   &body body)
  (declare (indent 1))
  (let ((files (gensym "files-"))
        (o-o-do-not-descend (gensym "do-not-descend-"))
        (more-files (gensym "more-files-")))
    `(let ((,files (fold (lambda (list file)
                           (cl-delete file list :test #'string-equal))
                         (directory-files ,root nil nil t)
                         '("." "..")))
           (,o-o-do-not-descend ,do-not-descend))
       ,@(cl-psetf do-not-descend o-o-do-not-descend)
       (while ,files
         (let ((,var (pop ,files)))
           ;; Actually, declarations should go here
           ;; but we probably should not worry about this.
           (cond
            ((file-symlink-p ,var))
            ((file-directory-p ,var)
             (setf ,var (file-name-as-directory ,var))
             (unless (cl-member ,var ,do-not-descend :test #'string-equal)
               (let ((,more-files
                      (directory-files ,var nil
                                       (rx string-start
                                           (or (not ?.)
                                               (seq ?. (or (not ?.)
                                                           (seq ?.
                                                                anything)))))
                                       t)))
                 (while ,more-files
                   (npush-pop ,more-files ,files)
                   (setf (car ,files) (concat ,var (car ,files))))))))
           ,@body))
       ,return)))

(defun ensure-directory (dir) (make-directory dir t) dir)

(cl-defmacro with-filenames-relative-to (root names &body body)
  (declare (indent 2))
  (let ((root-g (gensym "root-")))
    `(let ((,root-g ,root))
       (cl-flet ((file-name-relative (filename)
                   (expand-file-name (replace-regexp-in-string
                                      (rx string-start (one-or-more ?/)) ""
                                      filename)
                                     ,root-g)))
         (let ,(mapcar (lambda (x) `(,x (when ,x (file-name-relative ,x))))
                       names)
           ,@body)))))

(cl-defun elisp-insert-header ( &key
                                name description local-variables
                                first-publication-year authors)
  (insert name)
  (when description (insert " --- " description))
  (when local-variables
    (insert ?\s " -*-" ?\s)
    (cl-loop
     for (key value . tail) on local-variables by #'cddr
     do (insert (ensure-string key) ": "
                (ensure-string value))
     (when tail (insert "; ")))
    (insert " -*-"))
  (when (or authors first-publication-year)
    (insert ?\n)
    (insert "Copyright (C)")
    (when first-publication-year
      (insert ?\s
              (let ((current-year (format-time-string "%Y")))
                (fif (not (string-equal
                           current-year
                           first-publication-year))
                  (lambda (first-year)
                    (concat first-year "--" current-year))
                  first-publication-year))))
    (when authors (insert ?\s authors))))

(defun insert-metadata (plist)
  (cl-flet ((ensure-string (s) (akater-misc-ensure-string s)))
    (let (keys values)
      (let ((maxlength
             (loop-over-pairs (key value) on plist
               do (push key keys) (push value values)
               and maximize (length (ensure-string key)))))
        (let ((start (point)) (length 0) value)
          (while values
            (save-excursion
              (insert (aprog1 (ensure-string (pop keys))
                        (setq length (length it)))
                      ?:)
              (cl-loop repeat (+ 1 (- maxlength length))
                       do (insert ?\s))
              (funcall (cl-typecase (setq value (pop values))
                         ((or number string) #'princ)
                         (list #'prin1))
                       value (current-buffer))
              (insert ?\n)
              (comment-region start (point)))))))))

(defmacro defconst-with-prefix (prefix &rest pairs)
  (declare (indent 1))
  (let ((prefix-name (ensure-string prefix)))
    `(cl-eval-when (:compile-toplevel :load-toplevel :execute)
       ,@(cl-loop for (key value) on pairs by #'cddr
                  collect `(defconst ,(intern (concat prefix-name "-"
                                                      (ensure-string key)))
                             ,value)))))
