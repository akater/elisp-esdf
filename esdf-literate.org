# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: esdf-literate
#+subtitle: Part of the =esdf= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle esdf-literate.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) BUG(b@/!) | DONE(d@)
#+todo: TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Require
#+begin_src emacs-lisp :results none
(eval-when-compile (require 'subr-x))
(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'eieio))
(eval-when-compile (require 'esdf-macs))
(require 'cl-macs) ; cl-typep
(require 'esdf-core)
(require 'esdf-elisp)                   ; esdf-elisp-autoloads-file
#+end_src

* TODO esdf-elisp-autoloads-file is defined in esdf-elisp while most is defined in esdf-core
- State "TODO"       from              [2021-07-14 Wed 21:02] \\
  This is not good if we hope for esdf-literate to be ported as extension to ASDF.
* Order of actions that involve tangling
An untangled file may be tangled into multiple files, and, dually, a single compileable file could in principle be assembled from a multitude of untangled files.  This happens rarely but we still must address this.  Even if this is not permitted in literate programming, it is conceivable in real systems (I use this routinely already in Elisp packages).

In the worst case we have arbitrary family of compileable files tangled from arbitrary family of untangled (tangleable) files.  “Compiling a tangleable file” does not even amount to “compiling all its outputs” as outputs may depend on other tangleable files which should all be tangled before we may compile outputs.  Overall, “compiling a tangleable file” is meaningless.  One can only compile a system that involves tangling, and all tangling in the system must happen prior to any compilation, like this:

#+begin_example emacs-lisp
(cl-defmethod esdf-component-dependencies append ((o esdf-prepare-op)
                                                  (s esdf-literate-system))
  "All tangling must happen prior to any compilation."
  (ignore o)
  (list `(esdf-tangle-op ,s)))
#+end_example

However, this leads to another problem: the plan made with ~esdf-make-plan~ will not contain actions involving newly tangled components because they do not exist when the plan is computed.

Note also that compiling org file that is read as is, in the spirit of ~literate-lisp~, is, in contrast to all this, a well defined operation.  Tangling is not involved at all, and literate programming protocols are simply irrelevant, which suggests that it is indeed correct to separate this mode of operation from “literate programming” altogether.

One way to resolve this is to specify which source files to compile in order to “compile a tangleable file”, in the tangleable file component itself.  But we can't specify non-existing components in defsystem, only file paths.  And, once again, “compiling a tangleable file” is meaningless and should thus be avoided entirely.

I heard that ASDF might support multi-phase plans in future but so far it's not there.

We thus need to define our own method(s) that recomputes (parts of?) the plan.  We will register the newly generated components manually in the system.

We will define our own plan class which will include tangled source files, and reevaluate plans until all tangling is done.  Only so far, there is no “until”.  We simply tangle everything that is tangleable, once.

* literate-component
** Definition
#+begin_src emacs-lisp :results none
(defclass esdf-literate-component (esdf-component) ())
#+end_src

#+begin_src emacs-lisp :results none
(defclass esdf-literate-system (esdf-literate-component esdf-system)
  ((first-publication-year-as-string :initform (format-time-string "%Y"))))
#+end_src

#+begin_src emacs-lisp :results none
(defclass esdf-literate-module (esdf-literate-component esdf-module) ())
#+end_src

** Tests
*** TEST-PASSED Read definition of a literate system
#+begin_src emacs-lisp :tangle no :results output :wrap example emacs-lisp
(cl-print-object
 (esdf-component-tree
  (esdf-defsystem "esdf-simple-test-literate-system"
    :class esdf-literate-system
    :description "A sample system for testing esdf-literate"
    :author "=#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>="
    :license "GPL-3"
    :version "99999999"
    ;; esdf should likely support simply “live”
    ;; we use Portage as role model when it comes to dependencies
    :dependencies ((:bdepend (≥ "app-editors/emacs-25")
                             "app-emacs/akater-misc"
                             "app-emacs/anaphora")
                   (:rdepend (≥ "app-editors/emacs-25")
                             "app-emacs/akater-misc")
                   (:depend (test? ("app-emacs/akater-misc"))))
    :components ((:org "simple-test-system-core")
                 (:file "simple-test-system")
                 ( :module :submodule :class esdf-literate-module
                   :components
                   ((:file "simple-submodule-util")
                    (:org "simple-submodule-core")
                    (:org "simple-submodule-interface")
                    (:file "simple-submodule"))))
    :out-of-source "build"))
 nil)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
(#<esdf-org-untangled-file "esdf-simple-test-literate-system" "simple-test-system-core"> #<esdf-elisp-source-file "esdf-simple-test-literate-system" "simple-test-system"> (#<esdf-elisp-source-file "esdf-simple-test-literate-system" "submodule" "simple-submodule-util"> #<esdf-org-untangled-file "esdf-simple-test-literate-system" "submodule" "simple-submodule-core"> #<esdf-org-untangled-file "esdf-simple-test-literate-system" "submodule" "simple-submodule-interface"> #<esdf-elisp-source-file "esdf-simple-test-literate-system" "submodule" "simple-submodule">))
#+end_example

*** TEST-PASSED Path to component in a submodule
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(let ((test-esdf-tree
       (esdf-component-tree
        (esdf-defsystem "esdf-simple-test-literate-system"
          :class esdf-literate-system
          :description "A sample system for testing esdf-literate"
          :author "=#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>="
          :license "GPL-3"
          :version "99999999"
          ;; esdf should likely support simply “live”
          ;; we use Portage as role model when it comes to dependencies
          :dependencies ((:bdepend (≥ "app-editors/emacs-25")
                                   "app-emacs/akater-misc"
                                   "app-emacs/anaphora")
                         (:rdepend (≥ "app-editors/emacs-25")
                                   "app-emacs/akater-misc")
                         (:depend (test? ("app-emacs/akater-misc"))))
          :components ((:org "simple-test-system-core")
                       (:file "simple-test-system")
                       ( :module :submodule :class esdf-literate-module
                         :components
                         ((:file "simple-submodule-util")
                          (:org "simple-submodule-core")
                          (:org "simple-submodule-interface")
                          (:file "simple-submodule"))))
          :out-of-source "build"))))
  (cl-namestring (esdf-component-pathname (cadr (caddr test-esdf-tree)))))
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
"/home/akater/src/elisp-esdf/submodule/simple-submodule-core.org"
#+end_example

* untangled-file
** TODO Maybe inherit from ~esdf-static-file~ right here
- State "TODO"       from              [2022-09-09 Fri 02:08]
** Notes
We have to define this because we need to check type in [[eponymous-untangled-component-existsp]].

** Definition
#+begin_src emacs-lisp :results none
(defclass esdf-untangled-file (esdf-literate-component) ())
#+end_src

* tangle-op
** Prerequisites
*** filtered-downward-operation
**** Definition
#+begin_src emacs-lisp :results none
(defclass esdf-filtered-downward-operation (esdf-operation)
  ((filtered-downward-operation
    :initform nil :reader esdf-get-filtered-downward-operation
    :type esdf-operation-designator :allocation :class)
   ;; if we simply inherit esdf-downward-operation
   ;; then append method combination
   ;; makes both filtered and unfiltered versions apply
   (filter :type function :reader esdf-downward-filter :allocation :class))
  (:documentation "A FILTERED-DOWNWARD-OPERATION's dependencies propagate down the component hierarchy according to FILTER.
I.e., if O is a FILTERED-DOWNWARD-OPERATION and its FILTERED-DOWNWARD-OPERATION slot designates operation D, then
the action (O . M) of O on module M will depends on each of (D . C) for each child C of module M such that (FILTER C) is non-nil.
The default value for slot FILTERED-DOWNWARD-OPERATION is NIL, which designates the operation O itself.
E.g. in order for a literate MODULE to be tangled with TANGLE-OP, all the
children of the MODULE that are literate themselves must have been tangled with TANGLE-OP."))
#+end_src

or we could simply inherit esdf-downward-operation
but then the fact that we use append method combination
implies that we can only use :around method in methods specialized on filtered-downward-operation
otherwise everything is applied twice

#+begin_src emacs-lisp :tangle no :results none :eval never
(cl-defmethod esdf-component-dependencies append ((o esdf-filtered-downward-operation)
                                                  (c esdf-parent-component))
  (ignore o c)
  (error "Only :around method should be used for %s of operation of type %s"
         'esdf-component-dependencies 'esdf-filtered-downward-operation))
#+end_src

#+begin_src emacs-lisp :tangle no :results none :eval never
(defclass esdf-filtered-downward-operation (esdf-operation)
  ((filter :type function :reader esdf-downward-filter :allocation :class))
  (:documentation "A FILTERED-DOWNWARD-OPERATION's dependencies propagate down the component hierarchy according to FILTER.
I.e., if O is a FILTERED-DOWNWARD-OPERATION and its FILTERED-DOWNWARD-OPERATION slot designates operation D, then
the action (O . M) of O on module M will depends on each of (D . C) for each child C of module M such that (FILTER C) is non-nil.
The default value for slot FILTERED-DOWNWARD-OPERATION is NIL, which designates the operation O itself.
E.g. in order for a literate MODULE to be tangled with TANGLE-OP, all the
children of the MODULE that are literate themselves must have been tangled with TANGLE-OP."))
#+end_src

maybe downward-operation needs to be either breadth-first, or depth-first?

another approach is, do not use append method combination for component-dependencies but this seems to go explicitly against ASDF's idea of component-dependencies.

**** method component-dependencies filtered-downward-operation parent-component
#+begin_src emacs-lisp :results none
(cl-defmethod esdf-component-dependencies append ((o esdf-filtered-downward-operation)
                                                  (c esdf-parent-component))
  `((,(or (esdf-get-filtered-downward-operation o) o)
     ,@(cl-remove-if-not (esdf-downward-filter o)
                         (esdf-component-children c)))))
#+end_src

** Definition
#+begin_src emacs-lisp :results none
(defclass esdf-tangle-op (esdf-filtered-downward-operation)
  ((filter :initform (lambda (c) (cl-typep c 'esdf-literate-component))
           :allocation :class))
  (:documentation "Operation for tangling a component"))
#+end_src

Tangle is a “filtered downward operation”: tangling parent can only happen if tangleable chlidren are tangled.

Tangle-op should generally be called from configure-op?  This is an early idea of mine which is likely more suitable for application at esdf-ebuild level.

#+begin_src emacs-lisp :results none
(cl-defmethod initialize-instance :around ((o esdf-filtered-downward-operation)
                                           &key
                                           &allow-other-keys)
  (ignore o)
  (check-operation-constructor) nil
  ;; no check for inclusion in standard dependency propagation classes
  )
#+end_src

“perform tangle” must return the list of tangled files.

** Method component-dependencies tangle component
#+begin_src emacs-lisp :results none
(cl-defmethod esdf-component-dependencies append ((o esdf-tangle-op)
                                                  (c esdf-component))
  ;; no idea what it should be
  (ignore o c)
  nil)
#+end_src

* All tangling must happen prior to any compilation
Only the system as a whole can be tangled meaningfully.

** Method component-dependencies prepare literate-system
All tangling must happen prior to any compilation.
#+begin_src emacs-lisp :results none
(cl-defmethod esdf-component-dependencies append ((o esdf-prepare-op)
                                                  (s esdf-literate-system))
  (ignore o)
  (list `(esdf-tangle-op ,s)))
#+end_src

* TODO All tangling must happen after defining
- State "TODO"       from              [2021-06-23 Wed 08:00]
** An unsuccessful attempt
#+begin_src emacs-lisp :tangle no :results none
(defun esdf-component-name-equal (c1 c2)
  (string-equal (esdf-coerce-name c1)
                (esdf-coerce-name c2)))
#+end_src

#+begin_src emacs-lisp :tangle no :results none
(defun esdf-action-equal (a1 a2)
  (and (eq (car a1) (car a2))
       (set-equal (cdr a1) (cdr a2)
                  ;; this is a weak test
                  ;; but ASDF's default methods often operate with just names
                  :test #'esdf-component-name-equal)))
#+end_src

#+begin_src emacs-lisp :tangle no :results none
(cl-defmethod esdf-component-dependencies :around ((o esdf-prepare-op)
                                                   (s esdf-literate-system))
  (ignore o)
  (let ((define `(esdf-define-op ,s)))
    (nconc (list define `(esdf-tangle-op ,s))
           (cl-delete define (cl-call-next-method)
                      :test #'esdf-action-equal))))
#+end_src

* Inheritance of dependencies
For convenience, in-order-to dependencies for a tangleable file are inherited by all files tangled from it, with references to other tangleable files resolved into lists of references to corresponding files tangled from them.  In addition, compile-op and load-op of tangleable file are simultaneously set to compile-op and load-op of files tangled from it, respectively.  Thus, “compiling (loading) a tangleable file” means “compiling (loading) all files tangled from it” (in no specific order).

There might be a better approach but this is a natural first approximation.

* Plans for literate systems
** Overview
As described above, we recompute the plan.  This requires tangling while the file is made which is a little unfortunate but inevitable.

** BUG Tangles may still exist after a single tangle: e.g. tangleable files may be tangled
- State "BUG"        from              [2021-06-06 Sun 11:41] \\
  Not a severe bug (not likely to happen) but still.
** sequential-plan-eliminating-tangles
#+begin_src emacs-lisp :results none
(defclass esdf-sequential-plan-eliminating-tangles (esdf-sequential-plan)
  ()
  (:documentation "A plan that does not contain tangle operations."))
#+end_src

** TODO How does inheritance of in-order-to play with test-op?
- State "TODO"       from              [2021-06-16 Wed 03:51]
** tangle-options
#+begin_src emacs-lisp :results none
(defvar esdf-literate-tangle-options nil)
#+end_src

** insert-gpl-3-license
#+begin_src emacs-lisp :results none
(defun esdf-literate-insert-gpl-3-license
    (one-line-to-give-the-programs-name-and-an-idea-of-what-it-does
     initial-year-as-string authors &optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer (find-file-noselect "COPYING")
    (goto-char (point-min))
    (re-search-forward
     (rx line-start "How to Apply These Terms to Your New Programs"))
    (re-search-forward
     (rx line-start
         "<one line to give the program's name and a brief idea of what it does.>"))
    (let ((start (with-current-buffer buffer (point))))
      (with-current-buffer buffer
        (insert
         one-line-to-give-the-programs-name-and-an-idea-of-what-it-does ?\n))
      (re-search-forward (rx line-start))
      (let ((copyright-pattern
             (if (not (looking-at-p "Copyright"))
                 (error "Can't find copyright line in the template")
               (buffer-substring-no-properties
                (point) (progn (end-of-line) (point)))))
            (years (let ((current-year (format-time-string "%Y")))
                     (if (string-equal initial-year-as-string current-year)
                         current-year
                       (concat initial-year-as-string "--" current-year)))))
        (with-current-buffer buffer
          (insert
           (fold (lambda (string rule)
                   (replace-regexp-in-string (car rule) (cdr rule) string))
                 copyright-pattern
                 `((,(rx "<year>") . ,years)
                   (,(rx "<name of author>") . ,authors))))))
      (let ((rest-of-the-license (buffer-substring-no-properties
                                  (point)
                                  (progn (re-search-forward
                                          (rx line-start "Also add"))
                                         (beginning-of-line) (point)))))
        (with-current-buffer buffer
          (insert rest-of-the-license)
          (comment-region start (point)))))
    (kill-buffer)))
#+end_src

** eponymous-untangled-component-existsp
#+begin_src emacs-lisp :results none
(defun esdf-eponymous-untangled-component-existsp (child-component)
  (cl-check-type child-component esdf-child-component)
  (with-slots (name parent) child-component
    (cl-dolist (neighbour (esdf-component-children parent))
      (when (and (cl-typep neighbour 'esdf-untangled-file)
                 (string-equal name (esdf-component-name neighbour)))
        (cl-return neighbour)))))
#+end_src

** Method make-plan sequential-plan-eliminating-tangles operation literate-system
The current combination of options for ~with-esdf-session~ was obtained without understanding, by brute force.
#+begin_src emacs-lisp :results none
(cl-defmethod esdf-make-plan ((plan-class
                               (eql esdf-sequential-plan-eliminating-tangles))
                              (o esdf-operation)
                              (system esdf-literate-system)
                              &key forcing tangle-options)
  (ignore forcing)
  (with-esdf-session (:override-cache t)
    (let (tangles-table tangled-components unresolved-in-order-to)
      (with-esdf-session (:override-cache t :override t)
        (do-esdf-component-tree-proper (component system)
          ;; we might actually skip non-tangleable components here
          (dolist (spec (esdf-component-in-order-to component))
            (cl-loop
             for (_o . dependencies) in (cdr spec)
             do (do-sublists (sub dependencies)
                  (unless (cl-typep (car sub) 'esdf-component)
                    (setf (car sub)
                          (esdf-resolve-dependency-name component
                                                        (car sub)))))))
          ;; note: maybe we need to resolve names in sideway-dependencies slot too
          )
        (let ((plan (esdf-make-plan 'esdf-sequential-plan o system)))
          (if (eq 'tangled
                  (loop-over-esdf-actions (o . c) plan
                    do
                    (cond
                     ((cl-typep o 'esdf-define-op)
                      ;; actually, we better ensure such plans are not created
                      (esdf-perform o c)
                      (esdf-mark-as-done plan o c))
                     ((not (cl-typep o 'esdf-tangle-op))
                      (error "%s of %s %s does not start with tangles"
                             plan-class o c))
                     ((not (esdf-action-already-done-p plan o c))
                      (let ((new-components
                             (let ((esdf-literate-tangle-options tangle-options))
                               (esdf-perform-with-restarts o c))))
                        (esdf-mark-as-done plan o c)
                        (when new-components
                          (push (cons c new-components) tangles-table)
                          (dolist (new new-components)
                            (cl-pushnew new tangled-components :test #'eq)
                            (setf (alist-get new unresolved-in-order-to
                                             nil nil #'eq)
                                  (esdf-util-alist-nmerge
                                   (alist-get new unresolved-in-order-to
                                              nil nil #'eq)
                                   ;; these ↓ might be resolved to components right now
                                   ;; rather than in the beginning
                                   ;; but names should refer to tangled components if there's a conflict
                                   ;; so this would be fishy
                                   (esdf-component-in-order-to c)
                                   ;; the order of actions is sorta messed up
                                   ;; todo: we should likely merge rather than unite here as well
                                   #'cl-union)

                                  ;; here, we rely on compile untangled-file being in plans
                                  ;; that's undesirable
                                  (esdf-component-in-order-to c)
                                  (append (list `(esdf-compile-op (esdf-compile-op ,@new-components))
                                                `(esdf-load-op (esdf-load-op ,@new-components)))
                                          (esdf-component-in-order-to c))))))))
                    when (and (cl-typep o 'esdf-tangle-op) (cl-typep c 'esdf-system))
                    return 'tangled))
              plan
            (error "Can't make plan for literate system %s" system))))
      (cl-flet ((resolve (actions)
                  (cl-loop for (o . components) in actions
                           collect (cons o
                                         (amapcar (or (cdr (assoc it tangles-table))
                                                      it)
                                                  components)))))
        (dolist (new tangled-components)
          ;; the following likely needs to be in :after method
          ;; for tangle system
          (unless (cl-typep new 'esdf-elisp-autoloads-file)
            (let ((tangled-file-name (cl-namestring
                                      (esdf-component-pathname new))))
              (let ((name (file-name-nondirectory tangled-file-name))
                    (description
                     ;; for proper post-processing, we must keep link to the untangled files
                     ;; that produced this tangled file, or we must take post-processing details from the system definition
                     ;; but that means introducing special class(es)
                     ;; so for now we just search for eponymous untangled file
                     ;; for the references
                     (when-let ((eponymous-untangled-component
                                 (esdf-eponymous-untangled-component-existsp
                                  new)))
                       ;; we better insert such preamble
                       ;; when tangled file is empty
                       ;; also, this is different from what umake does:
                       ;; it takes the description from the first org file
                       ;; which tangled to the given file
                       ;; while we search for eponymous untangled file
                       ;; at the same level
                       ;; and take description from it, if found
                       (with-current-buffer (find-file-noselect
                                             (cl-namestring
                                              (esdf-component-pathname
                                               eponymous-untangled-component)))
                         (prog1 (when (re-search-forward
                                       (rx line-start "#+description: ")
                                       nil t)
                                  (buffer-substring-no-properties
                                   (point) (line-end-position)))
                           (kill-buffer)))))
                    (local-variables '(:lexical-binding t)))
                (with-current-buffer (find-file-noselect tangled-file-name)
                  (goto-char (point-min))
                  ;; todo: support different licenses
                  (when (string-equal "GPL-3" (slot-value system 'licence))
                    (esdf-literate-insert-gpl-3-license
                     (with-temp-buffer
                       (insert name)
                       (when description (insert " --- " description))
                       (when local-variables
                         (insert "  " "-*- ")
                         (cl-loop
                          for (key value) on local-variables by #'cddr
                          do (insert (esdf-util-ensure-string key) ": "
                                     (esdf-util-ensure-string value)))
                         (insert " -*-"))
                       (buffer-string))
                     (slot-value system 'first-publication-year-as-string)
                     (slot-value system 'author)))
                  (save-buffer)))
              (esdf-elisp-insert-postamble tangled-file-name)))
          ;; I used cl-macrolet because I thought
          ;; cl-flet would produce new closure for each tangled component
          (cl-macrolet ((delete-self (resolved-actions)
                          (let ((a (gensym "action-"))
                                (r (gensym "resolved-actions-")))
                            `(let ((,r ,resolved-actions))
                               (dolist (,a ,r (cl-delete
                                               ;; for cleaner output
                                               nil ,r :key #'cdr :test #'eq))
                                 (cl-symbol-macrolet ((components (cdr ,a)))
                                   (setf components (delq new components))))))))
            (setf (esdf-component-in-order-to new)
                  (cl-loop for (o . actions) in (cdr
                                                 (assoc new unresolved-in-order-to
                                                        #'eq))
                           ;; todo: use destructive map instead of cl-loop
                           collect (cons o (delete-self
                                            ;; prevent circular dependencies
                                            ;; on mutual tangles
                                            (resolve actions)))))))))
    (let ((plan (esdf-make-plan 'esdf-sequential-plan o system)))
      (esdf-traverse-action plan o system t)
      plan)))
#+end_src

If we don't use two different sessions, plan is not made properly.

** Tests
*** TEST-PASSED Many-source many-target tangles
The second tangle overwrites the results of the first one but that's irrelevant: we're testing system's internal dependency tree here.
#+begin_src emacs-lisp :tangle no :results output :wrap example emacs-lisp
(cl-print-object
 (esdf-plan-actions
  (let ((system (esdf-tests-ensure
                 "esdf-test-trivial-mutual-tangles-literate-system")))
    (esdf-make-plan 'esdf-sequential-plan-eliminating-tangles
                    (esdf-make-operation 'esdf-compile-op)
                    system)))
 nil)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
((#<esdf-prepare-op> . #<esdf-literate-system "esdf-test-trivial-mutual-tangles-literate-system">) (#<esdf-prepare-op> . #<esdf-org-untangled-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-1">) (#<esdf-prepare-op> . #<esdf-elisp-source-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-1">) (#<esdf-compile-op> . #<esdf-elisp-source-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-1">) (#<esdf-prepare-op> . #<esdf-elisp-source-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-2">) (#<esdf-compile-op> . #<esdf-elisp-source-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-2">) (#<esdf-compile-op> . #<esdf-org-untangled-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-1">) (#<esdf-prepare-op> . #<esdf-org-untangled-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-2">) (#<esdf-compile-op> . #<esdf-org-untangled-file "esdf-test-trivial-mutual-tangles-literate-system" "mutual-tangle-test-2">) (#<esdf-compile-op> . #<esdf-literate-system "esdf-test-trivial-mutual-tangles-literate-system">))
#+end_example

*** TEST-PASSED Preparing a literate system
Define is later than desired but this is already mentioned in esdf-literate.
#+begin_src emacs-lisp :tangle no :results output :wrap example emacs-lisp
(cl-print-object
 (esdf-plan-actions
  (esdf-make-plan 'esdf-sequential-plan
                  (esdf-make-operation 'esdf-prepare-op)
                  (esdf-tests-ensure "esdf-simple-test-literate-system")))
 nil)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
((#<esdf-tangle-op> . #<esdf-org-untangled-file "esdf-simple-test-literate-system" "simple-test-system-core">) (#<esdf-tangle-op> . #<esdf-org-untangled-file "esdf-simple-test-literate-system" "submodule" "simple-submodule-core">) (#<esdf-tangle-op> . #<esdf-org-untangled-file "esdf-simple-test-literate-system" "submodule" "simple-submodule-interface">) (#<esdf-tangle-op> . #<esdf-literate-module "esdf-simple-test-literate-system" "submodule">) (#<esdf-define-op> . #<esdf-literate-system "esdf-simple-test-literate-system">) (#<esdf-tangle-op> . #<esdf-literate-system "esdf-simple-test-literate-system">) (#<esdf-prepare-op> . #<esdf-literate-system "esdf-simple-test-literate-system">))
#+end_example

* Method operate operation literate-system
#+begin_src emacs-lisp :results none
(cl-defmethod esdf-operate ((operation esdf-operation)
                            (system esdf-literate-system)
                            &key plan-options tangle-options)
  "Perform OPERATION on SYSTEM after tangling the SYSTEM.

TANGLE-OPTIONS is a plist passed to all `esdf-perform' `esdf-tangle-op' methods called on SYSTEM during tangling."
  (let ((plan (apply 'esdf-make-plan 'esdf-sequential-plan-eliminating-tangles
                     operation system
                     :forcing (esdf-get-forcing *esdf-session*)
                     ;; since all tangles happen here in make-plan
                     ;; this is where we pass all options
                     :tangle-options tangle-options
                     plan-options)))
    (esdf-perform-plan plan)
    (cl-values operation plan)))
#+end_src
