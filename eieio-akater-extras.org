# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: eieio-akater-extras
#+subtitle: Little extensions and fixes for EIEIO
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle eieio-akater-extras.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) BUG(b@/!) | DONE(d@)
#+todo: TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)
# |

* Require
#+begin_src emacs-lisp :results none
(eval-when-compile (require 'cl-generic))
(eval-when-compile (require 'subr-x))
(require 'cl-generic)
(require 'cl-seq)
(require 'eieio-core)
(require 'eieio)
#+end_src

* Fixing excessive initform evaluations
:PROPERTIES:
:header-args:    :tangle (if (< emacs-major-version 28) "eieio-akater-extras.el" "")
:END:
** Summary
According to clhs,
#+begin_quote
A default initial value form for a slot is defined by using the :initform slot option to defclass. If no initialization argument associated with that slot is given as an argument to make-instance or is defaulted by :default-initargs, this default initial value form is evaluated in the lexical environment of the defclass form that defined it, and the resulting value is stored in the slot.
#+end_quote
--- [[clhs::07_a.htm][Object Creation and Initialization]]

This is not what happens in eieio.  Rather, initform is evaluated even if initarg is present.

** Emacs Lisp examples
Define a class with a slot that has an initarg and an initform involving undefined function:
#+begin_src emacs-lisp :tangle no :results none
(defclass test-initform ()
  ((test-slot :initarg :test-slot :initform (identity (non-existent)))))
#+end_src

Now,
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(condition-case err (make-instance 'test-initform :test-slot 0)
  (t err))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
(void-function non-existent)
#+end_example

Even though initform should not be evaluated since initarg is provided.

** Other references from CLHS:
#+begin_quote
If the initialization argument has a value in the initialization argument list, the value is stored into the slot of the newly created object, overriding any :initform form associated with the slot.
#+end_quote
--- [[clhs::07_aa.htm][Initialization Arguments]]

#+begin_quote
An :initform form is used to initialize a slot only if no initialization argument associated with that slot is given as an argument to make-instance or is defaulted by :default-initargs.
#+end_quote
--- [[clhs::07_ac.htm][Defaulting of Initialization Arguments]]

#+begin_quote
If a slot has both an :initform form and an :initarg slot option, and the initialization argument is defaulted using :default-initargs or is supplied to make-instance, the captured :initform form is neither used nor evaluated. 
#+end_quote
--- [[clhs::07_ad.htm][Rules for Initialization Arguments]]

** Common Lisp examples
Define a class in exactly the same way as in Emacs Lisp:
#+begin_src lisp :tangle no :results errors :wrap example lisp
(defclass test-initform ()
  ((test-slot :initarg :test-slot :initform (identity (non-existent)))))
#+end_src

#+RESULTS:
#+begin_example lisp
; in: DEFCLASS TEST-INITFORM
;     (NON-EXISTENT)
; 
; caught STYLE-WARNING:
;   undefined function: COMMON-LISP-USER::NON-EXISTENT
; 
; compilation unit finished
;   Undefined function:
;     NON-EXISTENT
;   caught 1 STYLE-WARNING condition
#+end_example
# You likely won't be able to reproduce this output but you get the idea

Appropriately,
#+begin_src lisp :tangle no :results value verbatim :wrap example lisp
(ignore-errors (make-instance 'test-initform))
#+end_src

#+RESULTS:
#+begin_example lisp
NIL
#<UNDEFINED-FUNCTION NON-EXISTENT {10052CF123}>
#+end_example

But with initarg, there is no need to evaluate initform, and it is not evaluated:
#+begin_src lisp :tangle no :results value verbatim :wrap example lisp
(make-instance 'test-initform :test-slot 0)
#+end_src

#+RESULTS:
#+begin_example lisp
#<TEST-INITFORM {1005224E13}>
#+end_example

** Notes
*** Initializing to quoted initform
Emacs 27 source claims
#+begin_quote
Paul Landes said in an email:
> CL evaluates it if it can, and otherwise, leaves it as
> the quoted thing as you already have.  This is by the
> Sonya E. Keene book and other things I've look at on the
> web.
#+end_quote
--- [[file:/usr/share/emacs/27.2/lisp/emacs-lisp/eieio.el::;; Paul Landes said in an email:][EIEIO on initform quoting]]

And eieio does quote initform forms with cars being symbols that are not fbound:
#+begin_src emacs-lisp :tangle no :results none
(defclass test-initform-quote ()
  ((test-slot :initform (non-existent))))
#+end_src

#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(slot-value (make-instance 'test-initform-quote) 'test-slot)
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
(non-existent)
#+end_example

There is however no reference to Sonya E. Keene or anything else.  Meanwhile,
#+begin_src lisp :tangle no :results errors :wrap example lisp
(defclass test-initform-quote ()
  ((test-slot :initform (non-existent))))
#+end_src

#+RESULTS:
#+begin_example lisp
; in: DEFCLASS TEST-INITFORM-QUOTE
;     (NON-EXISTENT)
; 
; caught STYLE-WARNING:
;   undefined function: COMMON-LISP-USER::NON-EXISTENT
; 
; compilation unit finished
;   Undefined function:
;     NON-EXISTENT
;   caught 1 STYLE-WARNING condition
#+end_example

#+begin_src lisp :tangle no :results value verbatim :wrap example emacs-lisp
(ignore-errors (make-instance 'test-initform-quote))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
NIL
#<UNDEFINED-FUNCTION NON-EXISTENT {1003251A33}>
#+end_example

Also, when discussing interplay between initform and default-initargs, Sonya E. Keene mentions:
#+begin_quote
The value of an :initform is evaluated each time it is used to initialize a slot.
#+end_quote
--- Sonya E. Keene, Object Oriented Programming in Common Lisp: A Programmer's Guide.  9.3. Controlling Initialization With ~defclass~ Options

Emacs 28 source removes the claim but adds
#+begin_quote
FIXME: Historically, EIEIO used a heuristic to try and guess
whether the initform is a form to be evaluated or just
a constant.  We use `eieio--eval-default-p' to see what the
heuristic says and if it disagrees with normal evaluation
then tweak the initform to make it fit and emit
a warning accordingly.
#+end_quote
--- [[file:~/src/emacs/lisp/emacs-lisp/eieio.el::;; FIXME: Historically, EIEIO used a heuristic to try and guess][eieio in git]]

Which arguably makes matters even more confusing.  There should be no such heuristic.

*** For the curious
We could not simply
#+begin_example elisp
(defclass test-initform ()
  ((test-slot :initarg :test-slot :initform (non-existent))))
#+end_example
as Elisp implementation of ~make-instance~ does not evaluate initform if its toplevel form's first symbol is not fbound.

Neither could we
#+begin_example elisp
(defclass test-initform ()
  ((test-slot :initarg :test-slot :initform (identity (undefined)))))
#+end_example
because ~undefined~ is actually defined in Elisp.

** Hotpatch
#+begin_src emacs-lisp :results none
(require 'eieio-custom)

  `(defclass eieio-widget-test-class nil
     ((a-string :initarg :a-string
	        :initform "The moose is loose"
	        :custom string
	        :label "Amorphous String"
	        :group (default foo)
	        :documentation "A string for testing custom.
This is the next line of documentation.")
      (listostuff :initarg :listostuff
	          :initform '("1" "2" "3")
	          :type list
	          :custom (repeat (string :tag "Stuff"))
	          :label "List of Strings"
	          :group foo
	          :documentation "A list of stuff.")
      (uninitialized :initarg :uninitialized
		     :type string
		     :custom string
		     :documentation "This slot is not initialized.
Used to make sure that custom doesn't barf when it encounters one
of these.")
      (a-number :initarg :a-number
	        :initform 2
	        :custom integer
	        :documentation "A number of thingies."))
     "A class for testing the widget on.")

;; only for Emacs 27?
(eval-after-load 'auth-source-pass
  `(defclass auth-source-backend ()
     ((type :initarg :type
            :initform 'netrc
            :type symbol
            :custom symbol
            :documentation "The backend type.")
      (source :initarg :source
              :type string
              :custom string
              :documentation "The backend source.")
      (host :initarg :host
            :initform t
            :type t
            :custom string
            :documentation "The backend host.")
      (user :initarg :user
            :initform t
            :type t
            :custom string
            :documentation "The backend user.")
      (port :initarg :port
            :initform t
            :type t
            :custom string
            :documentation "The backend protocol.")
      (data :initarg :data
            :initform nil
            :documentation "Internal backend data.")
      (create-function :initarg :create-function
                       :initform 'ignore
                       :type function
                       :custom function
                       :documentation "The create function.")
      (search-function :initarg :search-function
                       :initform 'ignore
                       :type function
                       :custom function
                       :documentation "The search function."))))

(cl-defmethod initialize-instance ((this eieio-default-superclass)
				   &optional slots)
  "Construct the new object THIS based on SLOTS.
SLOTS is a tagged list where odd numbered elements are tags, and
even numbered elements are the values to store in the tagged slot.
If you overload the `initialize-instance', there you will need to
call `shared-initialize' yourself, or you can call `call-next-method'
to have this constructor called automatically.  If these steps are
not taken, then new objects of your class will not have their values
dynamically set from SLOTS."
  ;; First, see if any of our defaults are `lambda', and
  ;; re-evaluate them and apply the value to our slots.
  (let* ((this-class (eieio--object-class this))
         (initargs slots)
         (slots (eieio--class-slots this-class)))
    (dotimes (i (length slots))
      ;; For each slot, see if we need to evaluate its initform.
      (let* ((slot (aref slots i))
             (slot-name (eieio-slot-descriptor-name slot))
             (initform (cl--slot-descriptor-initform slot)))
        (unless (or (eq eieio-unbound initform)
                    (when-let ((initarg
                                (car (rassq slot-name
                                            (eieio--class-initarg-tuples
                                             this-class)))))
                      (plist-get initargs initarg))
                    ;; Those slots whose initform is constant already have
                    ;; the right value set in the default-object.
                    (macroexp-const-p initform))
          ;; FIXME: We should be able to just do (aset this (+ i <cst>) dflt)!
          (eieio-oset this slot-name (eval initform t))))))
  ;; Shared initialize will parse our slots for us.
  (shared-initialize this slots))
#+end_src

* Covering buggy eql specilaizer
:PROPERTIES:
:header-args:    :tangle (if (< emacs-major-version 28) "eieio-akater-extras.el" "")
:END:
In ~(eql VAL)~ specializer, VAL should evaluate during macroexpansion of ~cl-defmethod~.

#+begin_quote
The parameter specializer name
~(eql eql-specializer-form)~ indicates that the corresponding argument must be
eql to the object that is the value of ~eql-specializer-form~ for the method
to be applicable. The ~eql-specializer-form~ is evaluated at the time that the
expansion of the defmethod macro is evaluated.
#+end_quote
--- [[clhs::m_defmet.htm][Macro DEFMETHOD]]

=cl-generic.el= does not do that prior to Emacs 28.  Here's a hack fix that seems to work, for older versions:

#+begin_src emacs-lisp :results none
(cl-defmethod cl-generic-generalizers ((specializer (head cl:eql)))
  "Support for (eql VAL) specializers.
These match if the argument is `eql' to VAL."
  (let ((value (eval (cadr specializer) t)))
    (setf (car specializer) 'eql)
    (puthash value specializer cl--generic-eql-used))
  (list cl--generic-eql-generalizer))
#+end_src

With eql specializer form not evaluated it does not seem possible to use a custom method combination.  The only way I can think of, is specializing ~cl-generic-combine-methods~ on the particular generic function:

#+begin_src emacs-lisp :tangle no :results none :eval never
(cl-defmethod cl-generic-combine-methods ((generic
                                           (cl:eql
                                            (cl--generic
                                             'my-generic-function)))
                                          methods)
  "Use `my-method-combination' method for generic function `my-generic-function'."
  (my-method-combination generic methods))
#+end_src

which is impossible if the specializer form is not evaluated.

See the actual example in [[“Append” method combination]] below.

* “Append” method combination
** Examples
*** Basic Examples
**** TEST-PASSED Symmetries (example by Edi Weitz from Common Lisp Recipes)
To assign method combination we use ~cl:eql~ specializer, see the previous section [[Covering buggy eql specilaizer]].
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(defclass my-test-quadrilateral () ())
(defclass my-test-kite (my-test-quadrilateral) ())
(defclass my-test-parallelogram (my-test-quadrilateral) ())
(defclass my-test-trapezoid (my-test-quadrilateral) ())
(defclass my-test-rhombus (my-test-kite my-test-parallelogram) ())
(defclass my-test-rectangle (my-test-parallelogram my-test-trapezoid) ())
(defclass my-test-square (my-test-rectangle my-test-rhombus) ())
(cl-defgeneric my-test-symmetries (shape))
(cl-defmethod cl-generic-combine-methods ((generic
                                           (cl:eql (cl--generic 'my-test-symmetries)))
                                          methods)
  "Use `append' operator method for generic function `my-test-symmetries'."
  (eieio-akater-extras-append-method-combination generic methods))
(cl-defmethod my-test-symmetries append ((shape my-test-quadrilateral))
  '(:identity))
(cl-defmethod my-test-symmetries append ((shape my-test-kite))
  '(:reflection-horizontal))
(cl-defmethod my-test-symmetries append ((shape my-test-parallelogram))
  '(:rotation-180-degrees))
(cl-defmethod my-test-symmetries append ((shape my-test-rhombus))
  '(:reflection-vertical))
(cl-defmethod my-test-symmetries append ((shape my-test-rectangle))
  '(:reflection-vertical :reflection-horizontal))
(cl-defmethod my-test-symmetries append ((shape my-test-square))
  '( :rotation-90-degrees :rotation-270-degree
     :reflection-diagonal-1 :reflection-diagonal-2))
(cl-values (my-test-symmetries (make-instance 'my-test-rectangle))
           (my-test-symmetries (make-instance 'my-test-rhombus)))
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
((:reflection-vertical :reflection-horizontal :rotation-180-degrees :identity)
 (:reflection-vertical :reflection-horizontal :rotation-180-degrees :identity))
#+end_example

** Prerequisites
*** fold
#+begin_src emacs-lisp :results none
(defun fold (f x seq) (cl-reduce f seq :initial-value x))
#+end_src

** Definition
#+begin_src emacs-lisp :results none
(defun eieio-akater-extras-append-method-combination (generic methods)
  (let ((mets-by-qual ()))
    (dolist (method methods)
      (let ((qualifiers (cl-method-qualifiers method)))
        (unless (member qualifiers '((:around) (append)))
          (error "Unsupported qualifiers in function %S: %S"
                 (cl--generic-name generic) qualifiers))
        (push method (alist-get (car qualifiers) mets-by-qual))))
    (cond
     ((null mets-by-qual)
      (lambda (&rest args)
        (apply #'cl-no-applicable-method generic args)))
     ((null (alist-get 'append mets-by-qual))
      (lambda (&rest args)
        (apply #'cl-no-primary-method generic args)))
     (t
      (let* ((primary-methods (cdr (assoc 'append mets-by-qual)))
             (primary-functions (mapcar (lambda (m)
                                          (cl-generic-call-method generic m))
                                        primary-methods))
             (f (lambda (&rest args)
                  (fold (lambda (x a) (append (apply a args) x))
                        nil primary-functions)))
             (around-methods (cdr (assoc :around mets-by-qual))))
        (dolist (method around-methods f)
          (setq f (cl-generic-call-method generic method f))))))))
#+end_src

** Tests
*** Multimethod
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(defclass my-test-operation () ())
(defclass my-test-downward-operation (my-test-operation)
  ((downward-operation :accessor my-test-get-downward-operation
                       ;; an alternative should be :allocation :class
                       :initarg :downward-operation
                       :initform nil)))
(defclass my-test-compile-op (my-test-downward-operation) ())
(defclass my-test-component ()
  ((name :accessor my-test-component-name
         :initarg :name)))
(defclass my-test-parent-component (my-test-component)
  ((children :accessor my-test-component-children
             :initarg :children
             :initform nil)))
(defclass my-test-system (my-test-parent-component) ())
(cl-defgeneric my-test-deps (operation component))
(cl-defmethod cl-generic-combine-methods ((generic
                                           (cl:eql (cl--generic 'my-test-deps)))
                                          methods)
  "Use `append' operator method for generic function `my-test-deps'."
  (eieio-akater-extras-append-method-combination generic methods))
(cl-defmethod my-test-deps append ((o my-test-downward-operation)
                                   (c my-test-parent-component))
  `((,(or (my-test-get-downward-operation o) o) ,@(my-test-component-children c))))
(my-test-deps (make-instance 'my-test-compile-op)
              (make-instance 'my-test-system
                             :name 'main
                             :children (list
                                        (make-instance 'my-test-system
                                                       :name 'sub-1)
                                        (make-instance 'my-test-system
                                                       :name 'sub-2))))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
((#s(my-test-compile-op nil)
    #s(my-test-system sub-1 nil)
    #s(my-test-system sub-2 nil)))
#+end_example

* or method combination
** Definition
#+begin_src emacs-lisp :results none
(defun eieio-akater-extras-or-method-combination (generic methods)
  (let ((mets-by-qual ()))
    (dolist (method methods)
      (let ((qualifiers (cl-method-qualifiers method)))
        (unless (member qualifiers '((:around) (or)))
          (error "Unsupported qualifiers in function %S: %S"
                 (cl--generic-name generic) qualifiers))
        (push method (alist-get (car qualifiers) mets-by-qual))))
    (cond
     ((null mets-by-qual)
      (lambda (&rest args)
        (apply #'cl-no-applicable-method generic args)))
     ((null (alist-get 'or mets-by-qual))
      (lambda (&rest args)
        (apply #'cl-no-primary-method generic args)))
     (t
      (let* ((primary-methods (cdr (assoc 'or mets-by-qual)))
             (primary-functions (mapcar (lambda (m)
                                          (cl-generic-call-method generic m))
                                        primary-methods))
             (f (lambda (&rest args)
                  (cl-some (lambda (f) (apply f args))
                           primary-functions)))
             (around-methods (cdr (assoc :around mets-by-qual))))
        (dolist (method around-methods f)
          (setq f (cl-generic-call-method generic method f))))))))
#+end_src

* reproduce-instance
** Notes
A well known implementation of ~reproduce-instance~ that does not employ ~make-instance~ requires ~allocate-instance~, ~reinitialize-instance~ and MOP.

Built-in ~clone~ better be called ~reproduce-instance~.

** Prerequisites
*** initargs-for-reproducing
**** TODO Define default method t t that collects initargs
- State "TODO"       from              [2021-07-01 Thu 07:01] \\
  With ~append~ method, those will go to the very end and may thus be overridden by users.
**** Notes
Such a generic function should have a usual append operator method, comoposed in usual order: from most specific method to the least specific one.

With MOP, we could give it a default method that goes through all slots of an instance that have initargs.  Other slots better be initialized with standard procedures (they do not yet exist in Elisp, sadly).

**** Definition
:PROPERTIES:
:header-args:    :tangle (if (>= emacs-major-version 28) "eieio-akater-extras.el" "")
:END:
#+begin_src emacs-lisp :results none
(cl-defgeneric eieio-akater-extras-initargs-for-reproducing (component)
  "Return initargs list to use when reproducing a `esdf-component' object.")
(cl-defmethod cl-generic-combine-methods ((generic
                                           (eql
                                            (cl--generic
                                             'eieio-akater-extras-initargs-for-reproducing)))
                                          methods)
  "Use `append' operator method for generic function `initargs-for-reproducing'."
  (eieio-akater-extras-append-method-combination generic methods))
#+end_src

**** (legacy) Definition
:PROPERTIES:
:header-args:    :tangle (if (< emacs-major-version 28) "eieio-akater-extras.el" "")
:END:
#+begin_src emacs-lisp :results none
(cl-defgeneric eieio-akater-extras-initargs-for-reproducing (component)
  "Return initargs list to use when reproducing a `esdf-component' object.")
(cl-defmethod cl-generic-combine-methods ((generic
                                           (cl:eql
                                            (cl--generic
                                             'eieio-akater-extras-initargs-for-reproducing)))
                                          methods)
  "Use `append' operator method for generic function `initargs-for-reproducing'."
  (eieio-akater-extras-append-method-combination generic methods))
#+end_src

** Definition
#+begin_src emacs-lisp :results none
(defun eieio-akater-extras-reproduce-instance (x &rest parameters)
  (apply #'make-instance (eieio-object-class x)
         (nconc parameters (eieio-akater-extras-initargs-for-reproducing x))))
#+end_src
